# Espresso Testing with Perfecto (Demo - Pipeline)aa
This project is cloned from [codelabs](http://www.code-labs.io/codelabs/android-testing/).  It does some basic unit and Espresso testing on a native Note taking application developed for Android.  It is possible to recreate the same project by following the instructions in codelabs along with the [Espresso Setup for Perfecto](http://developers.perfectomobile.com/display/PD/Perfecto+Gradle+Plugin).  If the intent is to use this project directly, then please note the following:
* This project was developed using Android Studio
* The file: [configFile.json](configFile.json) contains the perfecto specific device and reporting connection information.

## Configuration of Devices

```json
{
  "devices": [
    {"deviceName":"Enter_Device_Name"},
    {
      "platformName" : "Android",
      "platformVersion" : "^[6].*",
      "manufacturer" : "Samsung"
    }
  ],
  "apkPath": "./app/build/outputs/apk/mock/debug/app-mock-debug.apk",
  "testApkPath" : "./app/build/outputs/apk/androidTest/mock/debug/app-mock-debug-androidTest.apk",
  "projectName" : "{Enter_Project_Name}",
  "projectVersion" : "1.0",
  "jobName": "{Enter_Job_Name}",
  "jobNumber": 2,
  "tags": [
    "{tag1}", "{tag2}"
  ]
}

```



> As configured above, the tests will execute against 2 devices.  This can be expanded as needed

## Perfecto Lab Connection Information

* The Perfecto Lab Connection Settings are found in the [build.gradle](/app/build.gradle) file (this is the app level build.gradle file, not project level)
  * See the //TODO

```
apply plugin: 'com.android.application'
apply plugin: 'com.perfectomobile.instrumentedtest.gradleplugin'

android {
    compileSdkVersion rootProject.ext.compileSdkVersion
    buildToolsVersion rootProject.ext.buildToolsVersion

    perfectoGradleSettings {
        //TODO: Update CloudURL and Security Token to appropriate values for your cloud.
        configFileLocation "configFile.json"
        cloudURL "{yourcloud}.perfectomobile.com"
        securityToken "{Enter_Perfecto_Security_Token}"
    }

```

## Espresso Overview 1
In order to run the Espresso tests, you need to build both the application.apk file as well as the testing.apk file.

In this project the build files can be found under ([\Demo_Espresso\app\outputs\apk\app-mock-debug.apk](\Demo_Espresso\app\outputs\apk\app-mock-debug.apk) & [...\app-mock-debug-androidTest.apk](app\build\outputs\apk\app-mock-debug-androidTest.apk)

Both of these files are installed on the device during testing.

## To Execute the Expresso Tests from Command line:

```command
$ gradle perfecto-android-inst

```

* See [this page](http://developers.perfectomobile.com/display/PD/Configuration+Parameters+for+Gradle+Plugin) for additional parameters that can be added to the configFile.json or as command line arguments

## Adding Additional Tests

Navigate to the location that you want to add additional testing to

* On the NotesScreenTest File (app/src/androidTest/java/com.example.android.testing.notes/notes/NotesScreenTest.java)
Add something like: (this will add a test for special characters in the notes because why not...)

``` java
    @Test
    public void addNoteToNotesListSpecialCharacter() throws Exception {
        String newNoteTitle = "Special $&#*@";
        String newNoteDescription = "UI testing for Special Characters <br> *$#&(#(&@)!( <br> should show above ";

        // Click on the add note button
        onView(withId(R.id.fab_add_notes)).perform(click());

        // Add note title and description
        // Type new note title
        onView(withId(R.id.add_note_title)).perform(typeText(newNoteTitle), closeSoftKeyboard());
        onView(withId(R.id.add_note_description)).perform(typeText(newNoteDescription),
                closeSoftKeyboard()); // Type new note description and close the keyboard

        // Save the note
        onView(withId(R.id.fab_add_notes)).perform(click());

        // Scroll notes list to added note, by finding its description
        onView(withId(R.id.notes_list)).perform(
                scrollTo(hasDescendant(withText(newNoteDescription))));

        // Verify note is displayed on screen
        onView(withItemText(newNoteDescription)).check(matches(isDisplayed()));
    }
```

* Run the follwing Gradle commands to generate the .apk files

```
$ gradle build
$ gradle assembleAndroidTest
```

* Re-run the espresso suite to verify new test results are included

```
$ gradle perfecto-android-inst
```


## Test Results

* In DigitalZoom, you will find the tests with the full package and test name as a long string (ie. com.example.android.testing.notes.notes.AppNavigationTest#clickOnStatisticsNavigationItem_ShowsStatisticsScreen)
* Currently the tests are reported as single step, and are not detailed (however there is video of the execution)
* Test Result URL will be published at the conclusion of a run in the terminal window & will look something like the following

```

View the detailed report at:
https://demo.reporting.perfectomobile.com/library?tags[0]=91a4d8b7-d1d3-4b76-8d79-5c61b5bf2d84

Finished flow execution

```
## CI Integration (Jenkinsfile)
This project is integrated with Jenkins - will build on push to source
Additionally, the Jenkins Pipeline is defined in the [Jenkinsfile](Jenkinsfile), and can be modified to changes there.

* The Jenkins pipeline is broken into multiple steps currently
    * Setup - Performs JVM configuration and GitClone
    * Gradle Clean
    * Gradle Build
    * Gradle assembleAndroidTest
    * Run Espresso Test  
* Currently the Jenkinsfile is not configured to automatically uninstall and reinstall, that will be a TODO





# NOTES:

* Make sure that the build is set ot use the Debug "Build Variant" or the .apk produced won't install properly on a device
* If error on installing the .apk due to existing .apk with different signature.  Uninstall the .apk files from the device
```
$ adb -s dac64712 uninstall com.example.android.testing.notes.mock.test
$ adb -s dac64712 uninstall com.example.android.testing.notes.mock


```






